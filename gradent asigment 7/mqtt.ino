#define SOUND_SPEED 0.034
//Include Libraries
#include <SPI.h>
#include <WiFi.h>
#include <PubSubClient.h>
const char* ssid = "AndroidAP";
const char* password = "monicadz";
const char* mqttServer = "broker.emqx.io";
const int mqttPort = 1883;
const char* mqttUser = "charly";
const char* mqttPassword = "contrase";
const char *topic = "python/mqtt";

WiFiClient espClient;
PubSubClient client(espClient);

int datos[2];


const int trig =12;
const int echo =14;

int y = 0;

int pot =18;
float distancia_cm;
long tiempo;
/* structure that hold data*/
typedef struct{
  int sender;
  int datasensor;
  int counter;
  }Data;

xQueueHandle xQueue;
SemaphoreHandle_t xBinarySemaphore; 

void setup()
{ Serial.begin(115200);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  xBinarySemaphore = xSemaphoreCreateBinary();
  
  WiFi.begin(ssid, password);
  Serial.println("...................................");

  Serial.print("Connecting to WiFi.");
  while (WiFi.status() != WL_CONNECTED)
       {  delay(500);
          Serial.print(".") ;
       }
  Serial.println("Connected to the WiFi network");
  
  client.setServer(mqttServer, mqttPort);
  while (!client.connected())
  {      Serial.println("Connecting to MQTT...");
       if (client.connect("ESP32Client", mqttUser, mqttPassword))
           Serial.println("connected");
       else
       {   Serial.print("failed with state ");
           Serial.print(client.state());
           delay(2000);
       }
  }

  xQueue = xQueueCreate(5, sizeof(Data));
  xTaskCreatePinnedToCore(
      sendTask1,           /* Task function. */
      "sendTask1",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      sendTask2,           /* Task function. */
      "sendTask2",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      3,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,1); 
}

void loop(){  
 }

void sendTask1( void * parameter )
{ 
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data;
  /* sender 1 has id is 1 */
  data.sender = 1;

  for(;;){
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    data.datasensor = y = map(analogRead(15), 0, 4095, 0, 100);
    datos[0]= y ;


    //enviamos los datos
    //Serial.println("sendTask1 is sending data");
    /* send data to front of the queue */
    //Serial.println("\t\t\t\t Sensor 1 potenciometro - Adding "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /* check whether sending is ok or not */
    if( xStatus == pdPASS ) {
      /* increase counter of sender 1 */
      data.counter = data.counter + 1;
    }
    /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(2000));// Simulated interruption
  }
  vTaskDelete( NULL );
}

void sendTask2( void * parameter )
{
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  data.sender = 2;

  xSemaphoreGive(xBinarySemaphore);
  
  for(;;){
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
   digitalWrite(trig, LOW);  //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   
   digitalWrite(trig, HIGH);  //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(trig, LOW);
   
   tiempo = pulseIn(echo, HIGH);
   distancia_cm = tiempo * SOUND_SPEED/2;


    data.datasensor = distancia_cm;
    datos[1]= distancia_cm;
     
    //Serial.println("sendTask2 is sending data");
    //Serial.println("\t\t\t\t Sensor 2 ultrasonico cm - Adding "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    if( xStatus == pdPASS ) {
      data.counter = data.counter + 1;
    }
   /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(1000));// Simulated interruption
  }
  vTaskDelete( NULL );
}
void receiveTask( void * parameter )
{
  
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  for(;;){
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &data, xTicksToWait );
    /* check whether receiving is ok or not */
    //aqui se tiene que enviar los datos al broker
    client.loop();
     char str[40];
     char str2[40];
     sprintf(str, "%d", datos[1]);
     sprintf(str2, "%d", datos[0]);

     //client.publish("python/mqtt", str);
     //Serial.println(str);
     delay(500);
    if(xStatus == pdPASS){
      client.publish("python/mqtt", str);
      client.publish("python/mqtt", str2);
      
      Serial.println(str);
      Serial.println(str2);
      Serial.print("Datos Enviados:  ");
      //Serial.print(datos2);
//      Serial.println(str);
//      //Serial.print(datos[0]);
//      Serial.print("    ");
//      Serial.print(datos[1]);
    /*if(ok)
    {
      Serial.print("Datos Enviados:  ");
      Serial.print(datos[0]);
      Serial.print("    ");
      Serial.print(datos[1]);
      }
      else{
        Serial.println("no se han enviado");
        }*/
      }
  }
  vTaskDelete( NULL );
}

 
