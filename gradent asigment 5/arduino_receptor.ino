#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define CE_PIN 7
#define CSN_PIN 8

//create an RF24 object
//create an RF24 object
RF24 radio(CE_PIN, CSN_PIN);

//address through which two modules communicate.
const byte address[6] = "00015";
float datos[3];

void setup()
{
  while (!Serial);
    Serial.begin(9600);
  
  radio.begin();
  
  //set the address
  radio.openReadingPipe(0, address);
  
  //Set module as receiver
  radio.startListening();
}

void loop()
{
  uint8_t numero_canal;
  //Read the data if available in buffer
  if (radio.available())
  {
    radio.read(datos,sizeof(datos));
    
    Serial.print("Dato0= ");
    Serial.print(datos[0]);
    Serial.print("         ");
    Serial.print("Dato1= ");
    Serial.print(datos[1]);
    Serial.println("");   
  }
  else{
    Serial.println("No hay datos de radio disponibles");
    }
}
